const express = require("express");
const PORT = 2000;
const server = express();
const db = require("./models");
const redis = require("redis");

const redisClient = redis.createClient(6379);

(async () => {
  redisClient.on("error", (err) => {
    console.log("Redis Client Error", err);
  });
  redisClient.on("ready", () => console.log("Redis is ready"));

  await redisClient.connect();
  await redisClient.ping();
})();

server.use(express.json());

const { userSeqRoutes } = require("./routers");

server.use(express.static("./Public"));
server.use("/userSeq", userSeqRoutes);

server.listen(PORT, () => {
  db.sequelize.sync({ alter: true });
  console.log("Success Running at PORT: " + PORT);
});
