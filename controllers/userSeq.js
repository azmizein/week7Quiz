const db = require("../models");
const user = db.User;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const dbFilePath = "quiz.json";
const dbFilePath1 = "bukcet.json";
const jsonfile = require("jsonfile");
const { Redis } = require("ioredis");

const client = new Redis();
const answeredQuestions = new Set();
let lastUserName = "";

const calculateScore = (timeElapsed) => {
  if (timeElapsed <= 10) return 10;
  if (timeElapsed <= 20) return 9;
  if (timeElapsed <= 30) return 8;
  if (timeElapsed <= 40) return 7;
  if (timeElapsed <= 50) return 6;
  if (timeElapsed <= 60) return 5;
  return -5;
};

let currentQuestionIndex = 0;

module.exports = {
  register: async (req, res) => {
    try {
      const { name, phoneNumber, password } = req.body;

      if (!name || !phoneNumber || !password) {
        return res.status(400).send({
          message: "Form cannot be null",
        });
      }

      if (password.length < 8) throw "Minimum 8 characters";

      const salt = await bcrypt.genSalt(10);

      const hashPass = await bcrypt.hash(password, salt);

      const data = await user.create({
        name,
        phoneNumber,
        password: hashPass,
      });

      res.status(200).send({
        message: "Register Succes",
        data,
      });
    } catch (err) {
      res.status(400).send(err);
    }
  },

  login: async (req, res) => {
    try {
      const { name, password } = req.body;

      const isUserExist = await user.findOne({
        where: {
          name: name ? name : "",
        },
        raw: true,
      });

      if (!isUserExist) throw "User not found";

      const payload = {
        id: isUserExist.id,
        name: isUserExist.name,
        phoneNumber: isUserExist.phoneNumber,
        password: isUserExist.password,
        score: isUserExist.score,
        currentScore: isUserExist.currentScore,
      };
      const token = jwt.sign(payload, "azmi");

      const isValid = await bcrypt.compare(password, isUserExist.password);

      if (!isValid) throw `Wrong password`;

      res.status(200).send({
        message: "Login Succes",
        isUserExist,
        token,
      });
    } catch (err) {
      res.status(400).send(err);
    }
  },

  getQuiz: async (req, res) => {
    try {
      const data = jsonfile.readFileSync(dbFilePath);
      const quizQuestions = data.soal;

      const randomIndex = Math.floor(Math.random() * quizQuestions.length);
      const randomQuestion = quizQuestions[randomIndex];

      const currentTime = Date.now();
      const countdownDuration = 60;
      const questionId = randomQuestion.id;

      await client.hmset(
        `quiz:${questionId}`,
        "timestamp",
        currentTime,
        "countdown",
        countdownDuration
      );

      const simplifiedQuestion = {
        id: randomQuestion.id,
        type: randomQuestion.type,
        soal: randomQuestion.soal,
        answer: randomQuestion.answer,
      };

      res.status(200).json(simplifiedQuestion);
    } catch (err) {
      res.status(400).send(err);
    }
  },

  getBucket: async (req, res) => {
    try {
      const token = req.headers.authorization;

      if (!token) {
        return res.status(401).json({ message: "Unauthorized" });
      }

      const tokenParts = token.split(" ");
      if (tokenParts.length !== 2 || tokenParts[0] !== "Bearer") {
        return res.status(401).json({ message: "Invalid token format" });
      }

      const data = jsonfile.readFileSync(dbFilePath1);
      const bucket = data;

      const dataSoal = jsonfile.readFileSync(dbFilePath);
      const question = dataSoal;

      const verifyUser = await user.findOne({
        raw: true,
        where: {
          name: req.body.name,
        },
      });

      if (!verifyUser) {
        return res.status(404).json({ message: "User not found." });
      }
      const phoneNumberLastDigit = parseInt(
        verifyUser.phoneNumber.toString().slice(-1)
      );

      const roundNumberKey = `user:${verifyUser.name}:roundNumber`;
      let roundNumber = await client.get(roundNumberKey);

      if (roundNumber === null) {
        roundNumber = (phoneNumberLastDigit + 1).toString();
        await client.set(roundNumberKey, roundNumber);
      }

      const currentRound = `Round${roundNumber}`;

      if (!bucket.Round[currentRound]) {
        return res.status(400).json({ message: "Invalid round number." });
      }

      const quizz = bucket.Round[currentRound];
      const userQuestionIndexKey = `user:${verifyUser.name}:questionIndex`;
      let currentQuestionIndex = parseInt(
        await client.get(userQuestionIndexKey)
      );

      if (isNaN(currentQuestionIndex) || currentQuestionIndex >= quizz.length) {
        if (verifyUser.currentScore < 120) {
          await client.set(userQuestionIndexKey, "0");
          return res.status(400).json({ message: "Score too low to proceed." });
        }

        roundNumber++;
        await client.set(roundNumberKey, roundNumber.toString());
        await client.set(userQuestionIndexKey, "0");

        return res.status(400).json({
          message:
            "No more questions available for this round. Please click to next level.",
        });
      }

      const desiredQuestionIndex = quizz[currentQuestionIndex] - 1;
      const desiredQuestion = question.soal[desiredQuestionIndex];
      const questionId = desiredQuestion.id;

      const getBucketTimestamp = Date.now();

      await client.set(
        `quiz:${questionId}:timestamp`,
        getBucketTimestamp.toString()
      );

      if (!answeredQuestions.has(questionId)) {
        const simplifiedQuestion = {
          id: desiredQuestion.id,
          type: desiredQuestion.type,
          soal: desiredQuestion.soal,
          answer: desiredQuestion.answer,
        };
        return res.status(400).json(simplifiedQuestion);
      }

      answeredQuestions.add(questionId);

      const simplifiedQuestion = {
        id: desiredQuestion.id,
        type: desiredQuestion.type,
        soal: desiredQuestion.soal,
        answer: desiredQuestion.answer,
      };

      await client.set(
        userQuestionIndexKey,
        (currentQuestionIndex + 1).toString()
      );

      res.status(200).json(simplifiedQuestion);
    } catch (error) {
      console.log(error);
      res
        .status(400)
        .json({ message: "An error occurred while processing the answer." });
    }
  },

  postAnswer: async (req, res) => {
    try {
      let token = req.headers.authorization;

      if (!token) {
        return res.status(401).json({ message: "Unauthorized" });
      }

      token = token.split(" ")[1];

      let decoded = jwt.verify(token, "azmi");
      const data = jsonfile.readFileSync(dbFilePath);
      const quizQuestions = data.soal;
      const { questionId, selectedOption } = req.body;

      if (!questionId || !selectedOption) {
        return res
          .status(400)
          .json({ message: "Missing required parameters." });
      }

      const storedTimestamp = await client.get(`quiz:${questionId}:timestamp`);

      if (!storedTimestamp) {
        return res.status(400).json({ message: "Timestamp not found." });
      }

      const currentTime = Date.now();
      const timeElapsed = Math.floor(
        (currentTime - parseInt(storedTimestamp)) / 1000
      );

      if (!answeredQuestions.has(questionId)) {
        answeredQuestions.add(questionId);

        const question = quizQuestions.find((q) => q.id === questionId);

        if (!question) {
          return res.status(404).json({ message: "Question not found." });
        }

        let scoreChange = 0;

        if (selectedOption === question.correctOption) {
          scoreChange = calculateScore(timeElapsed);
        }

        const userToUpdate = await user.findByPk(decoded.id);

        if (!userToUpdate) {
          return res.status(404).json({ message: "User not found." });
        }

        const updatedScore = userToUpdate.currentScore + scoreChange;

        await userToUpdate.update({ currentScore: updatedScore });

        res.status(200).json({
          message:
            selectedOption === question.correctOption
              ? "Correct answer!"
              : "Wrong answer!",
          score: scoreChange,
        });
      } else {
        return res.status(400).json({ message: "Question already answered." });
      }
    } catch (err) {
      console.error(err);
      res
        .status(500)
        .json({ message: "An error occurred while processing the answer." });
    }
  },

  getLeaderboard: async (req, res) => {
    try {
      const catchRedis = await client.get("leaderboard");
      if (catchRedis) {
        return res.status(200).send(JSON.parse(catchRedis));
      }
      const users = await user.findAll({ raw: true });

      const leaderboard = users.map((users) => {
        return {
          name: users.name,
          score: users.score,
        };
      });
      await client.set("leaderboard", JSON.stringify(leaderboard));
      return res.status(200).send(leaderboard);
    } catch (err) {
      res.status(400).send(err);
    }
  },
};
