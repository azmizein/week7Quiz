const router = require("express").Router();
const { userSeq } = require("../controllers/index");

router.post("/register", userSeq.register);
router.post("/login", userSeq.login);
router.get("/getQuiz", userSeq.getQuiz);
router.get("/getLeaderboard", userSeq.getLeaderboard);
router.post("/answer", userSeq.postAnswer);
router.get("/bucket", userSeq.getBucket);

module.exports = router;
